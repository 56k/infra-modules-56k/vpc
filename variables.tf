/*
Input variables used to configure the 'vpc' module
*/

variable "app_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "availability_zones" {
  type = list
}

variable "vpc_public_cidr_blocks" {
  type = list
}

variable "vpc_private_cidr_blocks" {
  type = list
}
